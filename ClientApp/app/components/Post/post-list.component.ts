﻿import { Component, Inject, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";

@Component({
    selector: "post-list",
    templateUrl: './post-list.component.html',
    styleUrls: ['./post-list.component.css']
})

export class PostListComponent implements OnInit {
    
    @Input() class: string;
    title: string;
    selectedPost: Post;
    posts: Post[];

    constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private router: Router) {
    }

    ngOnInit() {
        console.log("PostListComponent " + "was created using class: " + this.class)
        var url = this.baseUrl + "api/post/";

        switch (this.class) {
            case "latest":
            default:
                this.title = "Latest posts";
                url += "Latest";
                break;

            case "byTitle":
                this.title = "By title posts";
                url += "ByTitle";
                break;

            case "random":
                this.title = "Random posts";
                url += "Random";
                break;
        }
        this.http.get<Post[]>(url).subscribe(result => { this.posts = result; }, error => console.error(error));
    }


    onSelect(post: Post) {
        this.selectedPost = post;
        console.log("Selected post " + this.selectedPost.Id);
        this.router.navigate(["post", this.selectedPost.Id])
    }
}

