﻿import { Component, Inject, OnInit } from "@angular/core";
import { FormGroup, FormControl, FormBuilder, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";

@Component({
    selector: "post-edit",
    templateUrl: './post-edit.component.html',
    styleUrls: ['./post-edit.component.css']
})

export class PostEditComponent {
    title: string;
    post: Post;
    form: FormGroup;

    //True -edit mode
    //False -create mode
    editMode: boolean;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private http: HttpClient, private fb: FormBuilder, @Inject('BASE_URL') private baseUrl: string) {
        this.post = <Post>{};
        this.createForm();

        var id = +this.activatedRoute.snapshot.params["id"];
        this.editMode = (this.activatedRoute.snapshot.url[1].path === "edit");
        if (this.editMode) {

            var url = this.baseUrl + "api/post/" + id;
            this.http.get<Post>(url).subscribe(result => { this.post = result; this.title = "Edycja - " + this.post.Title; }, error => console.error(error));
        }
        else {
            this.post.Id = id;
            this.title = "Create new post";
        }
    }

    onSubmit() {
        var tempPost = <Post>{};
        tempPost.Title = this.form.value.Title;
        tempPost.Description = this.form.value.Description;

        var url = this.baseUrl + "api/post";
        if (this.editMode) {
            tempPost.Id = this.post.Id;
            this.http
                .post<Post>(url, tempPost)
                .subscribe(result => { var v = result; console.log("Post " + v.Id + " was modified"); this.router.navigate(["home"]); }, error => console.error(error));
            this.updateForm();
        }
        else {
            this.http
                .put<Post>(url, tempPost)
                .subscribe(result => { var v = result; console.log("Post " + v.Id + " was added"); this.router.navigate(["home"]); }, error => console.error(error));
        }
    }

    onBack() {
        this.router.navigate(["home"]);
    }

    createForm() {
        this.form = this.fb.group({
            Title: ['', Validators.required],
            Description: ''
        });
    }

    updateForm() {
        this.form.setValue({
            Title: this.post.Title,
            Description: this.post.Description || ''
        });
    }

    getFormControl(name: string) {
        return this.form.get(name);
    }

    isValid(name: string) {
        var e = this.getFormControl(name);
        return e && e.valid;
    }

    hasError(name: string) {
        var e = this.getFormControl(name);
        return e && (e.dirty || e.touched) && !e.valid;
    }
}