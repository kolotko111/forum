﻿import { Component, Input } from "@angular/core";
import { Placeholder } from "@angular/compiler/src/i18n/i18n_ast";
@Component({
    selector: "post-search",
    templateUrl: './post-search.component.html',
    styleUrls: ['./post-search.component.css']
})

export class PostSearchComponent {
    @Input() class: string;
    @Input() placeholder: string;
}