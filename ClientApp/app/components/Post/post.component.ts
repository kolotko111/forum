﻿import { Component, Inject } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { AuthService } from '../../services/auth.service';

@Component({
    selector: "post",
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.css']
})
export class PostComponent {
    post: Post;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, public auth: AuthService) {
        this.post = <Post>{};
        var id = +this.activatedRoute.snapshot.params["id"];
        console.log(id);
        if (id) {
            var url = this.baseUrl + "api/post/" + id
            this.http.get<Post>(url).subscribe(result => { this.post = result; }, error => console.error(error));
        }
        else {
            console.log("Wrong id - go to home adress");
            this.router.navigate(["home"]);
        }
    }

    onEdit() {
        this.router.navigate(["post/edit", this.post.Id])
    }

    onDelete() {
        if (confirm("Do you want delete this post?")) {
            var url = this.baseUrl + "api/post/" + this.post.Id;
            this.http
                .delete(url)
                .subscribe(result => { var v = result; console.log("Post " + this.post.Id + " was deleted"); this.router.navigate(["home"]) }, error => console.error(error));
        }
    }
}