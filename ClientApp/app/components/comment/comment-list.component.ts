﻿import { Component, Inject, Input, OnChanges, SimpleChanges } from "@angular/core";
import { FormGroup, FormControl, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";


@Component({
    selector: "comment-list",
    templateUrl: './comment-list.component.html',
    styleUrls: ['./comment-list.component.css']
})

export class CommentListComponent implements OnChanges {
    @Input() post: Post;
    comments: Comment2[];
    comment2: Comment2;
    title: string;
    form: FormGroup;

    constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private router: Router) {
        this.comments = [];
        this.comment2 = <Comment2>{};
    }

    ngOnChanges(changes: SimpleChanges) {
        if (typeof changes['post'] !== "undefined") {
            var change = changes['post']

            if (!change.isFirstChange()) {
                this.loadData();
            }
        }
    }

    loadData() {
        var url = this.baseUrl + "api/comment/All/" + this.post.Id;
        this.http.get<Comment2[]>(url).subscribe(result => { this.comments = result; console.log(result); console.log(this.comments); }, error => console.log(error));
    }
        
    onCreate() {
        var url = this.baseUrl + "api/comment";
        this.comment2.PostId = this.post.Id;
        this.http
            .put<Comment2>(url, this.comment2)
            .subscribe(result => { var v = result; console.log("Comment was added " + v.Id); this.loadData(); }, error => console.log(error));
    }

    onSubmit() {
        var tempComment = <Comment2>{};
        tempComment.Description = this.form.value.Description;
        tempComment.Id = this.form.value.Id;
        tempComment.PostId = this.form.value.PostId;
        tempComment.Rating = this.form.value.Rating;

        var url = this.baseUrl + "api/comment";
        if (tempComment.PostId !== null) {
            this.http
                .post<Post>(url, tempComment)
                .subscribe(result => { var v = result; }, error => console.error(error));
            this.updateForm();
        }
        else {
            this.http
                .put<Post>(url, tempComment)
                .subscribe(result => { var v = result; }, error => console.error(error));
            this.updateForm();
        }

    }

    updateForm() {
        this.form.setValue({
            Description: this.comment2.Description || ''
        });
    }

    onEdit(comment: Comment2) {
        var url = this.baseUrl + "api/comment";
            this.http
                .post<Comment2>(url, comment)
                .subscribe(result => { var v = result; console.log("Comment was updated " + v.Id); this.loadData(); }, error => console.log(error))
    }

    onDelete(comment: Comment2) {
        if (confirm("Do you want delete this element?")) {
            var url = this.baseUrl + "api/comment/" + comment.Id;
            this.http
                .delete(url)
                .subscribe(result => { console.log("Comment " + comment.Id + " was deleted"); this.loadData(); }, error => console.log(error));
        }
    }

    getFormControl(name: string) {
        return this.form.get(name);
    }

    isValid(name: string) {
        var e = this.getFormControl(name);
        return e && e.valid;
    }

    hasError(name: string) {
        var e = this.getFormControl(name);
        return e && (e.dirty || e.touched) && !e.valid;
    }
}