﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rozdzial01_02.ViewModel
{
    public class TokenResponseViewModel
    {
        #region Konstruktor
        public TokenResponseViewModel()
        {

        }
        #endregion

        #region Właściwości
        public string token { get; set; }
        public int expiration { get; set; }
        #endregion
    }
}
