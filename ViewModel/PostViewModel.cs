﻿using Rozdzial01_02.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rozdzial01_02.ViewModel
{
    public class PostViewModel
    {
        #region Constructor 
        public PostViewModel()
        {

        }

        public PostViewModel(Post post)
        {
            Id = post.Id;
            Title = post.Title;
            Description = post.Description;
            CreatedDate = post.CreatedDate;
        }

        public PostViewModel(Post[] post)
        {
            foreach (var item in post)
            {
                Id = item.Id;
                Title = item.Title;
                Description = item.Description;
                CreatedDate = item.CreatedDate;
            }
            
        }
        #endregion

        #region parameters
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        #endregion
    }
}
