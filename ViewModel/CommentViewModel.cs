﻿using Rozdzial01_02.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rozdzial01_02.ViewModel
{
    public class CommentViewModel
    {
        #region Constructor 
        public CommentViewModel()
        {

        }

        public CommentViewModel(Comment comment)
        {
            Id = comment.Id;
            Description = comment.Description;
            CreatedDate = comment.CreatedDate;
            PostId = comment.Id;
            Post = comment.Post;
            AuthorId = comment.AuthorId;
            Author = comment.Author;
        }

        public CommentViewModel(Comment[] comment)
        {
            foreach (var item in comment)
            {
                Id = item.Id;
                Description = item.Description;
                Rating = item.Rating;
                CreatedDate = item.CreatedDate;
                PostId = item.Id;
                Post = item.Post;
                AuthorId = item.AuthorId;
                Author = item.Author;
            }

        }
        #endregion

        #region parameters
        public int Id { get; set; }
        public string Description { get; set; }
        public int Rating { get; set; }
        public DateTime CreatedDate { get; set; }
        public int PostId { get; set; }
        public Post Post { get; set; }
        public string AuthorId { get; set; }
        public ApplicationUser Author { get; set; }
        #endregion
    }
}
