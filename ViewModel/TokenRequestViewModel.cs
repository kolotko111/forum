﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rozdzial01_02.ViewModel
{
    public class TokenRequestViewModel
    {
        #region Konstruktor
        public TokenRequestViewModel()
        {

        }
        #endregion

        #region Właściwości
        public string grant_type { get; set; }
        public string client_id { get; set; }
        public string client_secret { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        #endregion
    }
}
