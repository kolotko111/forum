﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Rozdzial01_02.Model;
using Rozdzial01_02.ViewModel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Rozdzial01_02.Controllers
{
    public class PostController : BaseApiController
    {
        #region Constructor
        public PostController(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager, IConfiguration configuration) : base(context, roleManager, userManager, configuration)
        {

        }
        #endregion

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var post = DbContext.Post.Where(i => i.Id == id).FirstOrDefault();

            if (post == null)
            {
                return NotFound(new{Error = String.Format("Post {0} not found", id)});
            }

            var postViewModel = new PostViewModel(post);
            return new JsonResult(postViewModel,JsonSettings);
        }

        /// <summary>
        /// Add new Post to Db
        /// </summary>
        /// <param name="model">object PostViewModel contain data to put in db</param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        public IActionResult Put([FromBody]PostViewModel model)
        {
            if (model == null)
            {
                return new StatusCodeResult(500);
            }

            var post = new Post();

            post.Title = model.Title;
            post.Description = model.Description;
            post.CreatedDate = DateTime.Now;
            post.AuthorId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            post.Author = DbContext.Users.Where(q => q.Id == post.AuthorId).FirstOrDefault();

            DbContext.Post.Add(post);
            DbContext.SaveChanges();

            PostViewModel modelView = new PostViewModel(post);
            return new JsonResult(modelView, JsonSettings);
        }

        [HttpPost]
        [Authorize]
        public IActionResult Post([FromBody]PostViewModel model)
        {
            if (model == null)
            {
                return new StatusCodeResult(500);
            }

            var post = DbContext.Post.Where(q => q.Id == model.Id).FirstOrDefault();
            if (post == null)
            {
                return NotFound(new { Error = string.Format("Post not found {0}", model.Id) });
            }

            post.Title = model.Title;
            post.Description = model.Description;

            DbContext.SaveChanges();

            PostViewModel modelView = new PostViewModel(post);
            return new JsonResult(modelView, JsonSettings);
        }

        [HttpDelete("{id}")]
        [Authorize]
        public IActionResult Delete(int id)
        {
            var post = DbContext.Post.Where(q => q.Id == id).FirstOrDefault();
            if (post == null)
            {
                return NotFound(new { Error = string.Format("Post not found {0}", id) });
            }

            DbContext.Post.Remove(post);
            DbContext.SaveChanges();

            return new NoContentResult();
        }

        [HttpGet("Latest/{num?}")]
        public IActionResult Latest(int num = 10)
        {
            var latest = DbContext.Post.OrderByDescending(q => q.CreatedDate).Take(num).ToList();
            List<PostViewModel> postViewModel = new List<PostViewModel>();

            foreach (var item in latest)
            {
                var placeHolder = new PostViewModel(item);
                postViewModel.Add(placeHolder);
            }


            return new JsonResult(postViewModel,JsonSettings);
        }

        [HttpGet("ByTitle/{num:int?}")]
        public IActionResult ByTitle(int num = 10)
        {
            var byTitle = DbContext.Post.OrderBy(q => q.Title).Take(num).ToList();
            List<PostViewModel> postViewModel = new List<PostViewModel>();

            foreach (var item in byTitle)
            {
                var placeHolder = new PostViewModel(item);
                postViewModel.Add(placeHolder);
            }

            return new JsonResult(postViewModel, JsonSettings);
        }

        [HttpGet("Random/{num:int?}")]
        public IActionResult Random(int num = 10)
        {
            var random = DbContext.Post.OrderBy(q => Guid.NewGuid()).Take(num).ToList();
            List<PostViewModel> postViewModel = new List<PostViewModel>();

            foreach (var item in random)
            {
                var placeHolder = new PostViewModel(item);
                postViewModel.Add(placeHolder);
            }

            return new JsonResult(postViewModel, JsonSettings);
        }
    }
}
