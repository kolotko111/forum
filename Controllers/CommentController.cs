﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Rozdzial01_02.Model;
using Rozdzial01_02.ViewModel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Rozdzial01_02.Controllers
{
    [Route("api/[controller]")]
    public class CommentController : BaseApiController
    {
        #region Constructor
        public CommentController(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager, IConfiguration configuration) : base(context, roleManager, userManager, configuration)
        {

        }
        #endregion


        // GET api/<controller>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var comment = DbContext.Comment.Where(q => q.Id == id).FirstOrDefault();
            if (comment == null)
            {
                return NotFound(new {Error = string.Format("Post not found {0}", id)});
            }

            CommentViewModel modelView = new CommentViewModel(comment);
            return new JsonResult(modelView, JsonSettings);
        }

        // POST api/<controller>
        [HttpPost]
        [Authorize]
        public IActionResult Post([FromBody]CommentViewModel model)
        {
            if (model == null)
            {
                return StatusCode(500);
            }

            var comment = DbContext.Comment.Where(q => q.Id == model.Id).FirstOrDefault();
            if (comment == null)
            {
                return NotFound(new { Error = string.Format("Post not found {0}", model.Id) });
            }

            comment.Description = model.Description;

            DbContext.SaveChanges();

            CommentViewModel modelView = new CommentViewModel(comment);
            return new JsonResult(modelView, JsonSettings);
        }

        // PUT api/<controller>/5
        [HttpPut]
        [Authorize]
        public IActionResult Put([FromBody]CommentViewModel model)
        {
            if (model == null)
            {
                return StatusCode(500);
            }

            var comment = new Comment();
            comment.Description = model.Description;
            comment.Rating = 0;
            comment.CreatedDate = DateTime.Now;

            comment.PostId = model.PostId;
            comment.Post = DbContext.Post.Where(q => q.Id == comment.PostId).FirstOrDefault();

            comment.AuthorId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            comment.Author = DbContext.Users.Where(q => q.Id == comment.AuthorId).FirstOrDefault();

            DbContext.Comment.Add(comment);
            DbContext.SaveChanges();

            CommentViewModel modelView = new CommentViewModel(comment);
            return new JsonResult(modelView, JsonSettings);
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        [Authorize]
        public IActionResult Delete(int id)
        {
            var comment = DbContext.Comment.Where(q => q.Id == id).FirstOrDefault();
            if (comment == null)
            {
                return NotFound(new { Error = string.Format("Post not found {0}", id) });
            }

            DbContext.Comment.Remove(comment);
            DbContext.SaveChanges();

            return new NoContentResult();
        }

        [HttpGet("All/{postId}")]
        public IActionResult All(int postId)
        {
            var comment = DbContext.Comment.Where(q => q.PostId == postId).ToArray();
            if (comment == null)
            {
                return NotFound(new { Error = string.Format("Post not found {0}", postId) });
            }
            List<CommentViewModel> commentViewModel = new List<CommentViewModel>();

            foreach (var item in comment)
            {
                var placeHolder = new CommentViewModel(item);
                commentViewModel.Add(placeHolder);
            }

            return new JsonResult(commentViewModel, JsonSettings);
        }
    }
}
