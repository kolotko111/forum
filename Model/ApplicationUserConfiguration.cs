﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rozdzial01_02.Model
{
    public class ApplicationUserConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            builder.HasMany(q => q.Posts)
                   .WithOne(q => q.Author)
                   .HasForeignKey(q => q.AuthorId)
                   .OnDelete(DeleteBehavior.SetNull);

            builder.HasMany(q => q.Comments)
                   .WithOne(q => q.Author)
                   .HasForeignKey(q => q.AuthorId)
                   .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
