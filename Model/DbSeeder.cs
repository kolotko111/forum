﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rozdzial01_02.Model
{
    public class DbSeeder
    {
        #region Public method
        public static void Seed(ApplicationDbContext dbContext, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            if (!dbContext.Users.Any())
            {
                CreateUsers(dbContext, roleManager, userManager)
                    .GetAwaiter()
                    .GetResult();
            }

            if (!dbContext.Post.Any())
            {
                CreatePostsCommentsHashTags(dbContext);
            }
        }
        #endregion

        #region Generating methods
        private static async Task CreateUsers(ApplicationDbContext dbContext, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            string role_Administrator = "Administrator";
            string role_RegisteredUser = "RegisteredUser";

            if (!await roleManager.RoleExistsAsync(role_RegisteredUser))
            {
                await roleManager.CreateAsync(new IdentityRole(role_Administrator));
            }

            if (!await roleManager.RoleExistsAsync(role_RegisteredUser))
            {
                await roleManager.CreateAsync(new IdentityRole(role_RegisteredUser));
            }

            var user_Admin = new ApplicationUser()
            {
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = "Admin",
                Email = "admin@wp.pl",
                CreatedDate = DateTime.Now
            };

            if (await userManager.FindByNameAsync(user_Admin.UserName) == null)
            {
                await userManager.CreateAsync(user_Admin, "Pass4Admin");
                await userManager.AddToRoleAsync(user_Admin, role_RegisteredUser);
                await userManager.AddToRoleAsync(user_Admin, role_Administrator);
                user_Admin.EmailConfirmed = true;
                user_Admin.LockoutEnabled = false;
            }

#if DEBUG
            var user_Tomek = new ApplicationUser()
            {
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = "Tomek",
                Email = "tomek@wp.pl",
                CreatedDate = DateTime.Now
            };

            if (await userManager.FindByNameAsync(user_Tomek.UserName) == null)
            {
                await userManager.CreateAsync(user_Tomek, "ZAQ!2wsx");
                await userManager.AddToRoleAsync(user_Tomek, role_RegisteredUser);
                user_Admin.EmailConfirmed = true;
                user_Admin.LockoutEnabled = false;
            }

            var user_Darek = new ApplicationUser()
            {
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = "Darek",
                Email = "darek@wp.pl",
                CreatedDate = DateTime.Now
            };

            if (await userManager.FindByNameAsync(user_Darek.UserName) == null)
            {
                await userManager.CreateAsync(user_Darek, "ZAQ!2wsx");
                await userManager.AddToRoleAsync(user_Darek, role_RegisteredUser);
                user_Admin.EmailConfirmed = true;
                user_Admin.LockoutEnabled = false;
            }

            var user_Kacper = new ApplicationUser()
            {
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = "Kacper",
                Email = "kacper@wp.pl",
                CreatedDate = DateTime.Now
            };

            if (await userManager.FindByNameAsync(user_Kacper.UserName) == null)
            {
                await userManager.CreateAsync(user_Kacper, "ZAQ!2wsx");
                await userManager.AddToRoleAsync(user_Kacper, role_RegisteredUser);
                user_Admin.EmailConfirmed = true;
                user_Admin.LockoutEnabled = false;
            }

            var user_Mirek = new ApplicationUser()
            {
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = "Mirek",
                Email = "mirek@wp.pl",
                CreatedDate = DateTime.Now
            };

            if (await userManager.FindByNameAsync(user_Mirek.UserName) == null)
            {
                await userManager.CreateAsync(user_Mirek, "ZAQ!2wsx");
                await userManager.AddToRoleAsync(user_Mirek, role_RegisteredUser);
                user_Admin.EmailConfirmed = true;
                user_Admin.LockoutEnabled = false;
            }

#endif

            await dbContext.SaveChangesAsync();
        }

        private static void CreatePostsCommentsHashTags(ApplicationDbContext dbContext)
        {

#if DEBUG
            var num = 47;
            for (int i = 1; i <= num; i++)
            {
                var postAuthor = dbContext.Users.OrderBy(t => Guid.NewGuid()).FirstOrDefault();
                CreateSamplePosts(dbContext, postAuthor.Id, i);

                var commentAuthor = dbContext.Users.OrderBy(t => Guid.NewGuid()).FirstOrDefault();
                CreateSampleComments(dbContext, commentAuthor.Id, i, i);

                var hashTagAuthor = dbContext.Users.OrderBy(t => Guid.NewGuid()).FirstOrDefault();
                CreateSampleHashTags(dbContext, hashTagAuthor.Id, i);


            }

            for (int i = 1; i < num; i++)
            {
                AddHashTagsToPost(dbContext, i);
            }
#endif
        }
        #endregion

        #region Auxiliary methods 
        private static void CreateSamplePosts(ApplicationDbContext dbContext, string authorId, int num)
        {
            var author = dbContext.Users.Where(q => q.Id == authorId).FirstOrDefault();
            var post = new Post()
            {
                AuthorId = author.Id,
                Author = author,
                Title = string.Format("Tytył postu {0}", num),
                Description = string.Format("Opis {0}", num),
                CreatedDate = DateTime.Now
            };
            dbContext.Post.Add(post);
            dbContext.SaveChanges();

        }

        private static void CreateSampleComments(ApplicationDbContext dbContext, string authorId, int num, int postId)
        {
            Random rnd = new Random();
            var author = dbContext.Users.Where(q => q.Id == authorId).FirstOrDefault();
            var post = dbContext.Post.Where(q => q.Id == postId).FirstOrDefault();
            var comment = new Comment()
            {
                Description = string.Format("Przykładowy opis {0}", num),
                Rating = rnd.Next(0, 20),
                CreatedDate = DateTime.Now,
                PostId = post.Id,
                Post = post,
                AuthorId = author.Id,
                Author = author
            };
            dbContext.Comment.Add(comment);
            dbContext.SaveChanges();
        }

        private static void CreateSampleHashTags(ApplicationDbContext dbContext, string authorId, int num)
        {
            var hashTag = new HashTag()
            {
                Name = string.Format("HashTag {0}", num)
            };
            dbContext.HashTag.Add(hashTag);
            dbContext.SaveChanges();
        }

        private static void AddHashTagsToPost(ApplicationDbContext dbContext, int num)
        {
            var post = dbContext.Post.Where(q => q.Id == num).FirstOrDefault();
            var random = new Random();

            for (int i = 1; i < 5; i++)
            {
                int selectedValue = 0;
                var count = dbContext.HashTag.Count();
                switch (i)
                {
                    case 1:
                        selectedValue = random.Next(1, 10);
                        break;
                    case 2:
                        selectedValue = random.Next(11, 20);
                        break;
                    case 3:
                        selectedValue = random.Next(21, 30);
                        break;
                    case 4:
                        selectedValue = random.Next(31, 40);
                        break;
                }
                var hashTag = dbContext.HashTag.Where(q => q.Id == selectedValue).FirstOrDefault();
                var postHashTags = new PostHashTag();
                postHashTags.HashTagId = hashTag.Id;
                postHashTags.PostId = post.Id;
                dbContext.PostHashTag.Add(postHashTags);
                dbContext.SaveChanges();

            }
        }
        #endregion
    
    }
}
