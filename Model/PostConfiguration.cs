﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rozdzial01_02.Model
{
    public class PostConfiguration : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.HasMany(q => q.Comments)
                    .WithOne(q => q.Post)
                    .HasForeignKey(q => q.PostId)
                    .OnDelete(DeleteBehavior.Cascade);

            builder.Property(q => q.Title)
                   .IsRequired();

            builder.Property(q => q.Description)
                   .IsRequired();

            builder.Property(q => q.CreatedDate)
                   .IsRequired();
        }
    }
}
