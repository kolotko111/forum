﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rozdzial01_02.Model
{
    public class Comment
    {
        #region Constructor
        public Comment()
        {

        }
        #endregion

        #region parameters
        public int Id { get; set; }
        public string Description { get; set; }
        public int Rating { get; set; }
        public DateTime CreatedDate { get; set; }
        public int PostId { get; set; }
        public Post Post { get; set; }
        public string AuthorId { get; set; }
        public ApplicationUser Author { get; set; }
        #endregion
    }
}
