﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rozdzial01_02.Model
{
    public class PostHashTag
    {
        #region Constructor 
        public PostHashTag()
        {

        }
        #endregion

        #region parameters
        public int PostId { get; set; }
        public Post Post { get; set; }


        public int HashTagId { get; set; }
        public HashTag HashTag { get; set; }
        #endregion
    }
}
