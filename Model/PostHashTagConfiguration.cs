﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rozdzial01_02.Model
{
    public class PostHashTagConfiguration : IEntityTypeConfiguration<PostHashTag>
    {
        public void Configure(EntityTypeBuilder<PostHashTag> builder)
        {
            builder.HasKey(q => new { q.PostId, q.HashTagId });

            builder.HasOne(q => q.HashTag)
                   .WithMany(q => q.Posts)
                   .HasForeignKey(q => q.HashTagId);

            builder.HasOne(q => q.Post)
                   .WithMany(q => q.HashTags)
                   .HasForeignKey(q => q.PostId);
        }
    }
}
