﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rozdzial01_02.Model
{
    public class Post
    {
        #region Constructor 
        public Post()
        {

        }
        #endregion

        #region parameters
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<PostHashTag> HashTags { get; set; }
        public string AuthorId { get; set; }
        public ApplicationUser Author { get; set; }
        #endregion
    }
}
