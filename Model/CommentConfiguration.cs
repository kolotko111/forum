﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rozdzial01_02.Model
{
    public class CommentConfiguration : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.Property(q => q.Description)
                   .IsRequired();

            builder.Property(q => q.Rating)
                   .IsRequired();

            builder.Property(q => q.CreatedDate)
                   .IsRequired();

            builder.Property(q => q.PostId)
                   .IsRequired();
        }
    }
}
