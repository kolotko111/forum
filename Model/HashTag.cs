﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rozdzial01_02.Model
{
    public class HashTag
    {
        #region Constructor
        public HashTag()
        {

        }
        #endregion

        #region parameters
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<PostHashTag> Posts { get; set; }
        #endregion
    }
}
